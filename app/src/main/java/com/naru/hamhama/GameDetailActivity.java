package com.naru.hamhama;

import android.content.Intent;
import android.os.Bundle;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.naru.hamhama.fragment.CommentsListFragment;
import com.naru.hamhama.fragment.EventsListFragment;
import com.naru.hamhama.fragment.GameFragment;
import com.naru.hamhama.model.GamesEvents;
import com.naru.hamhama.viewholder.GameEventsViewHolder;
import com.naru.hamhama.viewholder.GameViewHolder;
import com.naru.hamhama.viewmodel.GameViewModel;


//TODO: To be replaced by fragment
public class GameDetailActivity extends AppCompatActivity {


    private GameDetailActivity gameDetailActivity;
    private FirebaseFirestore mFirestore;
    private DocumentReference mGameRef;
    private ListenerRegistration mGameRegistration;
    private RecyclerView mRecycler;
    private FirestoreRecyclerAdapter<GamesEvents,GameEventsViewHolder> mAdapter;
    private LinearLayoutManager mManager;
    private String mGameId;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_detail_layout);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        mGameId = extras.getString("KEY_GAME_ID");
        Log.d("idddd", mGameId);

        // Initialize Firestore
        mFirestore = FirebaseFirestore.getInstance();

        mGameRef =mFirestore.collection("game").document(mGameId);

        // Begin the transaction
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        GameFragment gameFragment = GameFragment.newInstance(mGameId);
        EventsListFragment eventsListFragment = EventsListFragment.newInstance(mGameId);
        // Replace the contents of the container with the new fragment
        ft.replace(R.id.events_list_fragment_container,eventsListFragment);
        ft.replace(R.id.header_fragment, gameFragment);
        // Complete the changes added above
        ft.commit();





    }


}