package com.naru.hamhama;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.naru.hamhama.fragment.NewsFragment;
import com.naru.hamhama.fragment.PlayersListFragment;
import com.naru.hamhama.fragment.SeasonFragment;

public class HomeActivity extends AppCompatActivity {

    final Fragment newsFragment = new NewsFragment();
    final Fragment playersListFragment = new PlayersListFragment();
    final Fragment seasonFragment = new SeasonFragment();
    Fragment active = newsFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        BottomNavigationView bottomNavigationView =findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(navListener);

        final FragmentManager fm = getSupportFragmentManager();

        fm.beginTransaction().add(R.id.fragment_container, seasonFragment, "3").commit();
        fm.beginTransaction().add(R.id.fragment_container, playersListFragment, "2").commit();
        fm.beginTransaction().add(R.id.fragment_container,newsFragment, "1").commit();



    }
    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    final FragmentManager fm = getSupportFragmentManager();

                    switch (item.getItemId()){
                        case R.id.navigation_news:
                            if(active != newsFragment)
                                fm.beginTransaction().show(newsFragment).commit();
                            else
                                fm.beginTransaction().hide(active).show(newsFragment).commit();
                            active = newsFragment;
                            break;

                        case R.id.navigation_players:
                            fm.beginTransaction().hide(active).show(playersListFragment).commit();
                            active = playersListFragment;
                            break;
                        case R.id.navigation_rank:
                            fm.beginTransaction().hide(active).show(seasonFragment).commit();
                            active = seasonFragment;
                            break;
                    }
                    return true;

                }
            };

}
