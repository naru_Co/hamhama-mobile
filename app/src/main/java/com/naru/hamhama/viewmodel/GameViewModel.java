package com.naru.hamhama.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.google.firebase.firestore.DocumentSnapshot;
import com.naru.hamhama.model.Games;

public class GameViewModel extends ViewModel {



    private final MutableLiveData<DocumentSnapshot> selectedArticle = new MutableLiveData<DocumentSnapshot>();

    public MutableLiveData<DocumentSnapshot> getSelectedArticle() {
        return selectedArticle;
    }

    public void setSelectedArticle(DocumentSnapshot snapshot) {
        selectedArticle.setValue(snapshot);
    }



}
