package com.naru.hamhama.viewholder;

import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.firestore.DocumentSnapshot;
import com.naru.hamhama.R;
import com.naru.hamhama.model.Comment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommentViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.comment_item_profile_image)
    public ImageView imageView;
    @BindView(R.id.comment_item_fullname)
    public TextView titleView;
    @BindView(R.id.comment_item_content)
    public TextView contentView;


    public CommentViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

    }

    public void bindToPost(final DocumentSnapshot snapshot) {
        Comment comment = snapshot.toObject(Comment.class);
        Resources resources = itemView.getResources();

        //Load Image
        Glide.with(imageView.getContext())
                .load(comment.getProfileImageUrl())
                .into(imageView);

        titleView.setText(comment.getUserName());
        contentView.setText(comment.getContent());



    }
}
