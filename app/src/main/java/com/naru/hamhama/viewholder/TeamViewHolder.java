package com.naru.hamhama.viewholder;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.firestore.DocumentSnapshot;
import com.naru.hamhama.R;
import com.naru.hamhama.model.Team;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TeamViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.team_item_logo)
    public ImageView imageView;
    @BindView(R.id.team_item_name)
    public TextView titleView;
    @BindView(R.id.team_item_rank)
    public TextView rankView;
    @BindView(R.id.team_item_mj)
    public TextView mjView;
    @BindView(R.id.team_item_pts)
    public TextView ptsView;
    private Context context;

    @BindView(R.id.team_item_card)
    public CardView cardView;

    public TeamViewHolder(View itemView, Context context) {
        super(itemView);
        this.context = context;
        ButterKnife.bind(this, itemView);

    }

    public void bindToPost(final DocumentSnapshot snapshot) {
        Team team = snapshot.toObject(Team.class);
        Resources resources = itemView.getResources();

        //Load Image
        Glide.with(imageView.getContext())
                .load(team.getLogoUrl())
                .into(imageView);

        titleView.setText(team.getShortName());
        rankView.setText(team.getRank().toString());
        Resources res = context.getResources();
        String match = res.getQuantityString(R.plurals.mj,team.getMj(),team.getMj().toString());
        mjView.setText(match);
        String points = res.getQuantityString(R.plurals.points,team.getPoints(),team.getPoints().toString());
        ptsView.setText(points);
        if (team.getShortName().equals("CSHL")) {
            cardView.setElevation(25);
            cardView.setCardElevation(25);
        }



    }
}
