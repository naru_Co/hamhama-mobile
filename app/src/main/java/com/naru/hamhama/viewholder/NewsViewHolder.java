package com.naru.hamhama.viewholder;

import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.firestore.DocumentSnapshot;
import com.naru.hamhama.R;
import com.naru.hamhama.model.News;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewsViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.news_item_image)
    public ImageView imageView;
    @BindView(R.id.new_item_title)
    public TextView titleView;
    @BindView(R.id.new_item_content)
    public TextView contentView;


    public NewsViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

    }

    public void bindToPost(final DocumentSnapshot snapshot) {
        News news = snapshot.toObject(News.class);
        Resources resources = itemView.getResources();

        //Load Image
        Glide.with(imageView.getContext())
                .load(news.getImage())
                .into(imageView);

        titleView.setText(news.getTitle());
        contentView.setText(news.getContent());



    }
}