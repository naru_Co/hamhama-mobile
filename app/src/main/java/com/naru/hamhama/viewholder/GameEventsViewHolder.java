package com.naru.hamhama.viewholder;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.firestore.DocumentSnapshot;
import com.naru.hamhama.R;
import com.naru.hamhama.model.Games;
import com.naru.hamhama.model.GamesEvents;

import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GameEventsViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.eventLogoView)
    public ImageView eventLogoView;
    @BindView(R.id.minuteView)
    public TextView minuteView;
    @BindView(R.id.hamhamaTextView)
    public TextView hamhamaTextView;
    @BindView(R.id.opponentTextView)
    public TextView opponentTextView;

    public GameEventsViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

    }

    public void bindToPost(final DocumentSnapshot snapshot) {
        GamesEvents gamesEvents = snapshot.toObject(GamesEvents.class);
        Resources resources = itemView.getResources();

        switch (gamesEvents.getEventState()){
            case 0:
                eventLogoView.setBackgroundResource(R.drawable.ic_football_whistle_of_referee);
                break;
            case 1:
                eventLogoView.setBackgroundResource(R.drawable.ic_football_warning_card_in_a_referee_hand);
                break;
            case 2:
                eventLogoView.setBackgroundResource(R.drawable.ic_football_red_card_in_a_hand);
                break;
            case 3:
                eventLogoView.setBackgroundResource(R.drawable.ic_up_and_down_arrows_inside_boxes);
                break;
            case 4:
                eventLogoView.setBackgroundResource(R.drawable.ic_football_ball);
                break;
        }
        minuteView.setText(gamesEvents.getMinute().toString()+"\'");
        switch (gamesEvents.getPosition()){
            case 1:
                hamhamaTextView.setText(gamesEvents.getEventText());
                break;
            case 2:
                opponentTextView.setText(gamesEvents.getEventText());
                break;
        }







    }
}
