package com.naru.hamhama.viewholder;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.firestore.DocumentSnapshot;
import com.naru.hamhama.R;
import com.naru.hamhama.model.Games;
import com.naru.hamhama.model.Team;

import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GameViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.game_item_opponent_logo)
    public ImageView opponentImageView;
    @BindView(R.id.game_item_date)
    public TextView gameDateView;
    @BindView(R.id.game_item_score)
    public TextView gameScoreView;
    @BindView(R.id.game_item_type)
    public TextView gameTypeView;

    private Context context;

    @BindView(R.id.team_item_card)
    public CardView cardView;

    public GameViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

    }

    public void bindToPost(final DocumentSnapshot snapshot) {
        Games game = snapshot.toObject(Games.class);
        Resources resources = itemView.getResources();

        //Load Image
        Glide.with(opponentImageView.getContext())
                .load(game.getOpponentLogoUrl())
                .into(opponentImageView);

        SimpleDateFormat simpleDate =  new SimpleDateFormat("dd/MM/yyyy");
        String strDt = simpleDate.format(game.getStartTime());
        gameDateView.setText(strDt);
        String score = game.getBm().toString() + ":" + game.getBe().toString();
        gameScoreView.setText(score);
        gameTypeView.setText(game.getTypeName());


    }
}
