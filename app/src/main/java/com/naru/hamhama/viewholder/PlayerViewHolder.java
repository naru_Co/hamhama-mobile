package com.naru.hamhama.viewholder;

import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.firestore.DocumentSnapshot;
import com.naru.hamhama.R;
import com.naru.hamhama.model.Player;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PlayerViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.player_item_image)
    public ImageView imageView;
    @BindView(R.id.player_item_name)
    public TextView nameView;
    @BindView(R.id.player_item_number)
    public TextView numberView;
    @BindView(R.id.player_item_position)
    public  TextView positionView;


    public PlayerViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

    }

    public void bindToPost(final DocumentSnapshot snapshot) {
        Player player = snapshot.toObject(Player.class);
        if (player.getName()==null || player.getNumber()==null || player.getPictureUrl()==null || player.getPosition()==null){

        }else {
            Resources resources = itemView.getResources();

            //Load Image
            Glide.with(imageView.getContext())
                    .load(player.getPictureUrl())
                    .into(imageView);

            nameView.setText(player.getName());
            numberView.setText(player.getNumber().toString());
            positionView.setText(player.getPosition());

        }


    }
}