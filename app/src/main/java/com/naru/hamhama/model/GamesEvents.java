package com.naru.hamhama.model;

import android.text.TextUtils;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class GamesEvents {

    private Integer eventState;
    private String eventText;
    private Integer minute;
    private String playerId;
    private Integer position;
    private @ServerTimestamp Date timestamp;

    public GamesEvents(){

    }
    public GamesEvents(Integer eventState, String eventText,Integer minute,String playerId,Integer position){
        this.eventState= eventState;
        this.eventText=eventText;
        this.minute=minute;
        this.playerId=playerId;
        this.position=position;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Integer getEventState() {
        return eventState;
    }

    public void setEventState(Integer eventState) {
        this.eventState = eventState;
    }

    public String getEventText() {
        return eventText;
    }

    public void setEventText(String eventText) {
        this.eventText = eventText;
    }

    public Integer getMinute() {
        return minute;
    }

    public void setMinute(Integer minute) {
        this.minute = minute;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
