package com.naru.hamhama.model;

import com.google.firebase.firestore.IgnoreExtraProperties;

import java.util.Date;

@IgnoreExtraProperties
public class Player {


    private String name;
    private Integer number;
    private String pictureUrl;
    private String position;
    private Date birthdate;
    private Boolean isActual;

    public Player(){}

    public Player(String name, Integer number, String pictureUrl, String position, Boolean isActual, Date birthdate) {
        this.name = name;
        this.number = number;
        this.pictureUrl = pictureUrl;
        this.position = position;
        this.isActual = isActual;
        this.birthdate = birthdate;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public Boolean getActual() {
        return isActual;
    }

    public void setActual(Boolean actual) {
        isActual = actual;
    }
}
