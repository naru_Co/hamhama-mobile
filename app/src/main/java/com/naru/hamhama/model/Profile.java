package com.naru.hamhama.model;

import android.text.TextUtils;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class Profile {

    private String userId;
    private Integer adminLevel;
    private @ServerTimestamp Date timestamp;

    public Profile() {

    }

    public Profile(String userId, Integer adminLevel) {
        this.userId = userId;
        this.adminLevel = adminLevel;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer isAdmin() {
        return adminLevel;
    }

    public void setAdmin(Integer adminLevel) {
        adminLevel = adminLevel;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
