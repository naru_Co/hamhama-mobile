package com.naru.hamhama.model;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.IgnoreExtraProperties;
import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

@IgnoreExtraProperties
public class News {

    public static final String FIELD_IMAGE = "image";
    public static final String FIELD_TITLE = "title";
    public static final String FIELD_CONTENT = "content";
    public static final String FIELD_AUTHOR = "author";
    public static final String FIELD_CREATION_DATE = "creationDate";

    private String title;
    private String content;
    private String image;
    private String author;
    private @ServerTimestamp Date creationDate;

    public News(){}

    public News(String title, String content, String image, FirebaseUser author, Date creationDate) {
        this.title = title;
        this.content = content;
        this.image = image;
        this.author = author.getUid();
        this.creationDate = creationDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }


}
