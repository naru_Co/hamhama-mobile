package com.naru.hamhama.model;
import com.google.firebase.firestore.IgnoreExtraProperties;


@IgnoreExtraProperties
public class Team {


    private String name;
    private Integer points;
    private String shortName;
    private String logoUrl;
    private Integer v;
    private Integer be;
    private Integer bm;
    private Integer d;
    private Integer mj;
    private Integer n;
    private Integer rank;

    public Team(){}


    public Team(String name,Integer points, String shortName, String logoUrl, Integer v, Integer be, Integer bm, Integer d, Integer mj, Integer n,Integer rank) {
        this.name = name;
        this.points = points;
        this.shortName = shortName;
        this.logoUrl = logoUrl;
        this.v = v;
        this.be=be;
        this.bm=bm;
        this.d=d;
        this.mj=mj;
        this.n=n;
        this.rank=rank;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public Integer getBe() {
        return be;
    }

    public void setBe(Integer be) {
        this.be = be;
    }

    public Integer getBm() {
        return bm;
    }

    public void setBm(Integer bm) {
        this.bm = bm;
    }

    public Integer getD() {
        return d;
    }

    public void setD(Integer d) {
        this.d = d;
    }

    public Integer getMj() {
        return mj;
    }

    public void setMj(Integer mj) {
        this.mj = mj;
    }

    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }
}
