package com.naru.hamhama.model;

import android.text.TextUtils;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class Comment {

    private String userId;
    private String userName;
    private String profileImageUrl;
    private String content;
    private @ServerTimestamp Date timestamp;

    public Comment(){

    }
    public Comment(FirebaseUser user, String content){
        this.userId= user.getUid();
        this.userName=user.getDisplayName();
        if(TextUtils.isEmpty(this.userName)){
            this.userName = user.getEmail();
        }
        this.profileImageUrl=user.getPhotoUrl().toString();
        this.content=content;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
