package com.naru.hamhama.model;

import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class Games {

    private Integer halfTime;
    private String oppenentId;
    private String refreeName;
    private String stade;
    private Integer stateId;
    private String stateName;
    private Integer be;
    private Integer bm;
    private Integer typeId;
    private String typeName;
    private String opponentLogoUrl;
    private String opponentShortName;
    private @ServerTimestamp Date creationDate;
    private @ServerTimestamp Date startTime;

    public Games(){

    }
    public Games(Date startTime,Integer be,Integer bm, String opponentLogoUrl, String opponentShortName, Integer halfTime, String oppenentId, String refreeName, String stade,
    Integer stateId,String stateName,Integer typeId,String typeName,Date creationDate ){
        this.halfTime= halfTime;
        this.oppenentId=oppenentId;
        this.refreeName=refreeName;
        this.be=be;
        this.bm=bm;
        this.startTime=startTime;
        this.creationDate=creationDate;
        this.stade=stade;
        this.stateId=stateId;
        this.stateName=stateName;
        this.typeId=typeId;
        this.typeName=typeName;
        this.opponentLogoUrl=opponentLogoUrl;
        this.opponentShortName=opponentShortName;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Integer getHalfTime() {
        return halfTime;
    }

    public Integer getBe() {
        return be;
    }

    public void setBe(Integer be) {
        this.be = be;
    }

    public Integer getBm() {
        return bm;
    }

    public void setBm(Integer bm) {
        this.bm = bm;
    }

    public String getOpponentLogoUrl() {
        return opponentLogoUrl;
    }

    public void setOpponentLogoUrl(String opponentLogoUrl) {
        this.opponentLogoUrl = opponentLogoUrl;
    }

    public String getOpponentShortName() {
        return opponentShortName;
    }

    public void setOpponentShortName(String opponentShortName) {
        this.opponentShortName = opponentShortName;
    }

    public void setHalfTime(Integer halfTime) {
        this.halfTime = halfTime;
    }

    public String getOppenentId() {
        return oppenentId;
    }

    public void setOppenentId(String oppenentId) {
        this.oppenentId = oppenentId;
    }

    public String getRefreeName() {
        return refreeName;
    }

    public void setRefreeName(String refreeName) {
        this.refreeName = refreeName;
    }

    public String getStade() {
        return stade;
    }

    public void setStade(String stade) {
        this.stade = stade;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
