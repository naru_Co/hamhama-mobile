package com.naru.hamhama;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.naru.hamhama.fragment.CommentFragment;
import com.naru.hamhama.fragment.CommentsListFragment;
import com.naru.hamhama.model.Comment;

//TODO: To be replaced by fragment
public class NewsDetailActivity extends AppCompatActivity implements CommentFragment.CommentListener {
    private String mTitle;
    private String mContent;
    private String mImage;
    private TextView mTitleView;
    private TextView mContentView;
    private ImageView mImageView;
    private String mNewsId;

    private CommentFragment mCommentFragment;

    private FirebaseFirestore mFirestore;
    private DocumentReference mComment;


    @Override
    public void onComment(Comment comment) {
        addComment(comment);

    }

    private void addComment(final Comment comment){
        Log.d("News ?", mNewsId);
        FirebaseFirestore db = mFirestore.getInstance();
        db.collection("news")
                .document(mNewsId).collection("comments")
                .add(comment);




    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        mTitle = extras.getString("TITLE");
        mContent = extras.getString("CONTENT");
        mImage = extras.getString("IMAGE");
        mNewsId = extras.getString("KEY_RESTAURANT_ID");

        mTitleView = findViewById(R.id.news_detail_title);
        mContentView = findViewById(R.id.news_detail_content);
        mImageView = findViewById(R.id.news_detail_image);
        mContentView.setText(mContent);
        mTitleView.setText(mTitle);

        getSupportActionBar().setSubtitle(mTitle);
        getSupportActionBar().setLogo(R.drawable.main_logo);
        Glide.with(mImageView.getContext())
                .load(mImage)
                .into(mImageView);

        // Begin the transaction
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        CommentsListFragment commentsListFragment = CommentsListFragment.newInstance(mNewsId);
        // Replace the contents of the container with the new fragment
        ft.replace(R.id.comment_list_fragment_container, commentsListFragment);
        // or ft.add(R.id.your_placeholder, new FooFragment());
        // Complete the changes added above
        ft.commit();

        //getComments


    }
}