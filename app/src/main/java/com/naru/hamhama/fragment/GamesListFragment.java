package com.naru.hamhama.fragment;


import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.naru.hamhama.GameDetailActivity;
import com.naru.hamhama.R;
import com.naru.hamhama.model.Games;

import com.naru.hamhama.viewholder.GameViewHolder;
import com.naru.hamhama.viewmodel.GameViewModel;


public class GamesListFragment extends Fragment {

    private FirebaseFirestore mFirestore;
    private Query mQuery;
    private RecyclerView mRecycler;
    private FirestoreRecyclerAdapter<Games, GameViewHolder> mAdapter;
    private LinearLayoutManager mManager;



    public GamesListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_game_list, container, false);
        // Inflate the layout for this fragment
        FirebaseFirestore.setLoggingEnabled(true);
        // Firestore
        mFirestore = FirebaseFirestore.getInstance();

        mRecycler = rootView.findViewById(R.id.game_list);
        mRecycler.setHasFixedSize(true);
        return rootView;

    }

    @Override
    public void onStart() {
        super.onStart();

        mAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAdapter != null) {
            mAdapter.stopListening();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



// Set up Layout Manager, reverse layout
        mManager = new LinearLayoutManager(getActivity());
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        mRecycler.setLayoutManager(mManager);






        mQuery = mFirestore.collection("game").orderBy("startTime");


        FirestoreRecyclerOptions<Games> options = new FirestoreRecyclerOptions.Builder<Games>()
                .setQuery(mQuery, Games.class)
                .build();

        mAdapter = new FirestoreRecyclerAdapter<Games, GameViewHolder>(options) {
            @Override
            public void onBindViewHolder(GameViewHolder holder, int position, Games model) {

                GameViewModel gameViewModel = ViewModelProviders.of(getActivity()).get(GameViewModel.class);
                holder.itemView.setOnClickListener((v)->{

                    //call intent
                    Intent intent = new Intent(getActivity(), GameDetailActivity.class);
                    Bundle extras = new Bundle();
                    extras.putString("KEY_GAME_ID",getSnapshots().getSnapshot(position).getId());
                    intent.putExtras(extras);
                    startActivity(intent);
                });


                // Bind Post to ViewHolder, setting OnClickListener for the star button
                holder.bindToPost(getSnapshots().getSnapshot(position));

            }

            @Override
            public GameViewHolder onCreateViewHolder(ViewGroup group, int i) {

                View view = LayoutInflater.from(group.getContext())
                        .inflate(R.layout.game_item, group, false);

                return new GameViewHolder(view);
            }
        };
        mRecycler.setAdapter(mAdapter);

    }



}
