package com.naru.hamhama.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.naru.hamhama.R;
import com.naru.hamhama.viewholder.GameViewHolder;
import com.naru.hamhama.viewmodel.GameViewModel;

import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class GameFragment extends Fragment {


    public GameFragment() {
        // Required empty public constructor
    }

    private FirebaseFirestore mFirestore;
    private Query mQuery;
    private RecyclerView mRecycler;

    public static GameFragment newInstance(String gameId) {
        Log.d("mmmmm", gameId);
        GameFragment gameFragment = new GameFragment();
        Bundle args = new Bundle();
        args.putString("KEY_GAME_ID", gameId);
        gameFragment.setArguments(args);
        return gameFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.game_item, container,false);

        mFirestore = FirebaseFirestore.getInstance();

        String gameId = getArguments().getString("KEY_GAME_ID");
        Log.d("fffffffffffffff", gameId);

        DocumentReference docRef = mFirestore.collection("game").document(gameId);

        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        GameViewHolder gameViewHolder= new GameViewHolder(rootView);
                        gameViewHolder.bindToPost(document);
                    } else {
                        Log.d("dddd", "No such document");
                    }
                } else {
                    Log.d("ddd", "get failed with ", task.getException());
                }
            }
        });

        return rootView;


    }
}
