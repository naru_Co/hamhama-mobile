package com.naru.hamhama.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.naru.hamhama.R;
import com.naru.hamhama.model.Comment;
import com.naru.hamhama.viewholder.CommentViewHolder;


public class CommentsListFragment extends Fragment {


    private static final String TAG = "CommentsListFragment";
    private static final int LIMIT = 7;
    private FirebaseFirestore mFirestore;
    private Query mQuery;
    private RecyclerView mRecycler;
    private FirestoreRecyclerAdapter<Comment, CommentViewHolder> mAdapter;
    private LinearLayoutManager mManager;



        // Creates a new fragment given an int and title
        public static CommentsListFragment newInstance(String newsId) {
            CommentsListFragment commentsListFragment = new CommentsListFragment();
            Bundle args = new Bundle();
            args.putString("NEWS_ID", newsId);
            commentsListFragment.setArguments(args);
            return commentsListFragment;
        }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_comments_list, container, false);
        // Enable Firestore logging
        FirebaseFirestore.setLoggingEnabled(true);
        // Firestore
        mFirestore = FirebaseFirestore.getInstance();

        mRecycler = rootView.findViewById(R.id.comment_list);
        return rootView;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Set up Layout Manager, reverse layout
        mManager = new LinearLayoutManager(getActivity());
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        mRecycler.setNestedScrollingEnabled(false);
        mRecycler.setLayoutManager(mManager);

        String newsId = getArguments().getString("NEWS_ID");


        mQuery = mFirestore.collection("news").document(newsId).collection("comments").orderBy("timestamp", Query.Direction.DESCENDING)
                .limit(LIMIT);

        FirestoreRecyclerOptions<Comment> options = new FirestoreRecyclerOptions.Builder<Comment>()
                .setQuery(mQuery, Comment.class)
                .build();

        mAdapter = new FirestoreRecyclerAdapter<Comment, CommentViewHolder>(options) {
            @Override
            public void onBindViewHolder(CommentViewHolder holder, int position, Comment model) {

                // Bind Post to ViewHolder, setting OnClickListener for the star button
                holder.bindToPost(getSnapshots().getSnapshot(position));

            }
            @Override
            public CommentViewHolder onCreateViewHolder(ViewGroup group, int i) {

                View view = LayoutInflater.from(group.getContext())
                        .inflate(R.layout.comment_item, group, false);

                return new CommentViewHolder(view);
            }
        };
        mRecycler.setAdapter(mAdapter);
        mAdapter.startListening();

    }


}
