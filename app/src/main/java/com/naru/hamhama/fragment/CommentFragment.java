package com.naru.hamhama.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.naru.hamhama.R;
import com.naru.hamhama.model.Comment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CommentFragment extends Fragment {

    public static final String TAG = "CommentFragment";

    @BindView(R.id.comment_create)
    EditText mComment;

    @BindView(R.id.send_comment)
    Button mSendComment;

    public interface CommentListener {

        void onComment(Comment comment);

    }
    private CommentListener mCommentListener;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_comment, container,false);
        ButterKnife.bind(this, rootView);

        mSendComment.setVisibility(View.GONE);
        mComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                showSubmitIfReady();

            }
        });


        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof CommentListener) {
            mCommentListener = (CommentListener) context;
        }




    }

    private void showSubmitIfReady() {
        boolean isReady = mComment.getText().toString().length()>3;
        if (isReady){
            mSendComment.setVisibility(View.VISIBLE);
        }else{
            mSendComment.setVisibility(View.GONE);
        }
    }


    @OnClick(R.id.send_comment)
    public void onSubmitClicked(View view){
        Comment comment = new Comment(
                FirebaseAuth.getInstance().getCurrentUser(),
                mComment.getText().toString()
        );
        mCommentListener.onComment(comment);

        mComment.setText("");
        mComment.clearFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

    }

}
