package com.naru.hamhama.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.naru.hamhama.R;
import com.naru.hamhama.model.Player;
import com.naru.hamhama.viewholder.PlayerViewHolder;

public class PlayersListFragment extends Fragment {

    private FirebaseFirestore mFirestore;
    private Query mQuery;
    private RecyclerView mRecycler;
    private FirestoreRecyclerAdapter<Player, PlayerViewHolder> mAdapter;
    private LinearLayoutManager mManager;



    public PlayersListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_players_list, container, false);
        // Inflate the layout for this fragment
        FirebaseFirestore.setLoggingEnabled(true);
        // Firestore
        mFirestore = FirebaseFirestore.getInstance();

        mRecycler = rootView.findViewById(R.id.player_list);
        mRecycler.setHasFixedSize(true);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        mAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAdapter != null) {
            mAdapter.stopListening();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



// Set up Layout Manager, reverse layout
        mManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true);
        mRecycler.setLayoutManager(mManager);






        mQuery = mFirestore.collection("players");


        FirestoreRecyclerOptions<Player> options = new FirestoreRecyclerOptions.Builder<Player>()
                .setQuery(mQuery, Player.class)
                .build();

        mAdapter = new FirestoreRecyclerAdapter<Player, PlayerViewHolder>(options) {
            @Override
            public void onBindViewHolder(PlayerViewHolder holder, int position, Player model) {


                // Bind Post to ViewHolder, setting OnClickListener for the star button
                holder.bindToPost(getSnapshots().getSnapshot(position));

            }

            @Override
            public PlayerViewHolder onCreateViewHolder(ViewGroup group, int i) {

                View view = LayoutInflater.from(group.getContext())
                        .inflate(R.layout.item_player, group, false);

                return new PlayerViewHolder(view);
            }
        };
        mRecycler.setAdapter(mAdapter);

    }



}
