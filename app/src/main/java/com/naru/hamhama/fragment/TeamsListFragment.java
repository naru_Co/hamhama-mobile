package com.naru.hamhama.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.naru.hamhama.R;
import com.naru.hamhama.model.Team;
import com.naru.hamhama.viewholder.TeamViewHolder;

public class TeamsListFragment extends Fragment {

    private FirebaseFirestore mFirestore;
    private Query mQuery;
    private RecyclerView mRecycler;
    private FirestoreRecyclerAdapter<Team, TeamViewHolder> mAdapter;
    private LinearLayoutManager mManager;



    public TeamsListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_team_list, container, false);
        // Inflate the layout for this fragment
        FirebaseFirestore.setLoggingEnabled(true);
        // Firestore
        mFirestore = FirebaseFirestore.getInstance();

        mRecycler = rootView.findViewById(R.id.team_list);
        mRecycler.setHasFixedSize(true);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        mAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAdapter != null) {
            mAdapter.stopListening();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



// Set up Layout Manager, reverse layout
        mManager = new LinearLayoutManager(getActivity());
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        mRecycler.setLayoutManager(mManager);






        mQuery = mFirestore.collection("teams").orderBy("points");


        FirestoreRecyclerOptions<Team> options = new FirestoreRecyclerOptions.Builder<Team>()
                .setQuery(mQuery, Team.class)
                .build();

        mAdapter = new FirestoreRecyclerAdapter<Team, TeamViewHolder>(options) {
            @Override
            public void onBindViewHolder(TeamViewHolder holder, int position, Team model) {


                // Bind Post to ViewHolder, setting OnClickListener for the star button
                holder.bindToPost(getSnapshots().getSnapshot(position));

            }

            @Override
            public TeamViewHolder onCreateViewHolder(ViewGroup group, int i) {

                View view = LayoutInflater.from(group.getContext())
                        .inflate(R.layout.team_item, group, false);

                return new TeamViewHolder(view,getContext());
            }
        };
        mRecycler.setAdapter(mAdapter);

    }



}
