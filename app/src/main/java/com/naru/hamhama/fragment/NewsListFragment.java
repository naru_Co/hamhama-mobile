package com.naru.hamhama.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.naru.hamhama.NewsDetailActivity;
import com.naru.hamhama.R;
import com.naru.hamhama.model.News;
import com.naru.hamhama.viewholder.NewsViewHolder;


public abstract class NewsListFragment extends Fragment  {



    private static final String TAG = "NewsFragment";
    private static final int LIMIT = 7;
    private FirebaseFirestore mFirestore;
    private Query mQuery;
    private RecyclerView mRecycler;
    private FirestoreRecyclerAdapter<News, NewsViewHolder> mAdapter;
    private LinearLayoutManager mManager;

    public NewsListFragment(){}


    @Override
    public View onCreateView (@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_news_list,container,false);

        // Enable Firestore logging
        FirebaseFirestore.setLoggingEnabled(true);
        // Firestore
        mFirestore = FirebaseFirestore.getInstance();

        mRecycler = rootView.findViewById(R.id.news_list);
        mRecycler.setHasFixedSize(true);





        return rootView;



    }

    @Override
    public void onStart() {
        super.onStart();

            mAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAdapter != null) {
            mAdapter.stopListening();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



// Set up Layout Manager, reverse layout
        mManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true);
        mRecycler.setLayoutManager(mManager);






        mQuery = mFirestore.collection("news").orderBy("creationDate",Query.Direction.DESCENDING)
                .limit(LIMIT);

        FirestoreRecyclerOptions<News> options = new FirestoreRecyclerOptions.Builder<News>()
                .setQuery(mQuery, News.class)
                .build();

        mAdapter = new FirestoreRecyclerAdapter<News, NewsViewHolder>(options) {
            @Override
            public void onBindViewHolder(NewsViewHolder holder, int position, News model) {

                holder.itemView.setOnClickListener((v)->{
                    Intent intent = new Intent(getActivity(), NewsDetailActivity.class);
                    Bundle extras = new Bundle();
                    extras.putString("TITLE",model.getTitle());
                    extras.putString("IMAGE",model.getImage());
                    extras.putString("CONTENT",model.getContent());
                    extras.putString("TRANSITION_NAME",model.getCreationDate().toString());
                    extras.putString("KEY_RESTAURANT_ID",getSnapshots().getSnapshot(position).getId());
                    intent.putExtras(extras);

                    holder.imageView.setTransitionName(model.getCreationDate().toString());
                    Log.d(TAG, model.getCreationDate().toString());
                    ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity() ,holder.imageView,ViewCompat.getTransitionName(holder.imageView));
                    startActivity(intent);


                });
                // Bind Post to ViewHolder, setting OnClickListener for the star button
                holder.bindToPost(getSnapshots().getSnapshot(position));

            }

            @Override
            public NewsViewHolder onCreateViewHolder(ViewGroup group, int i) {

                View view = LayoutInflater.from(group.getContext())
                        .inflate(R.layout.item_new, group, false);

                return new NewsViewHolder(view);
            }
        };
        mRecycler.setAdapter(mAdapter);

    }











}
