package com.naru.hamhama.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.naru.hamhama.R;
import com.naru.hamhama.model.Comment;
import com.naru.hamhama.model.GamesEvents;
import com.naru.hamhama.viewholder.CommentViewHolder;
import com.naru.hamhama.viewholder.GameEventsViewHolder;


public class EventsListFragment extends Fragment {


    private static final String TAG = "EventsListFragment";
    private static final int LIMIT = 7;
    private FirebaseFirestore mFirestore;
    private Query mQuery;
    private RecyclerView mRecycler;
    private FirestoreRecyclerAdapter<GamesEvents, GameEventsViewHolder> mAdapter;
    private LinearLayoutManager mManager;



        // Creates a new fragment given an int and title
        public static EventsListFragment newInstance(String gameId) {
            EventsListFragment eventsListFragment = new EventsListFragment();
            Bundle args = new Bundle();
            args.putString("GAME_ID", gameId);
            eventsListFragment.setArguments(args);
            return eventsListFragment;
        }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_events_list, container, false);
        // Enable Firestore logging
        FirebaseFirestore.setLoggingEnabled(true);
        // Firestore
        mFirestore = FirebaseFirestore.getInstance();

        mRecycler = rootView.findViewById(R.id.event_list);
        return rootView;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Set up Layout Manager, reverse layout
        mManager = new LinearLayoutManager(getActivity());
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        mRecycler.setNestedScrollingEnabled(false);
        mRecycler.setLayoutManager(mManager);

        String newsId = getArguments().getString("GAME_ID");


        mQuery = mFirestore.collection("game").document(newsId).collection("events").orderBy("minute", Query.Direction.DESCENDING);

        FirestoreRecyclerOptions<GamesEvents> options = new FirestoreRecyclerOptions.Builder<GamesEvents>()
                .setQuery(mQuery, GamesEvents.class)
                .build();

        mAdapter = new FirestoreRecyclerAdapter<GamesEvents, GameEventsViewHolder>(options) {
            @Override
            public void onBindViewHolder(GameEventsViewHolder holder, int position, GamesEvents model) {

                // Bind Post to ViewHolder, setting OnClickListener for the star button
                holder.bindToPost(getSnapshots().getSnapshot(position));

            }
            @Override
            public GameEventsViewHolder onCreateViewHolder(ViewGroup group, int i) {

                View view = LayoutInflater.from(group.getContext())
                        .inflate(R.layout.event_item, group, false);

                return new GameEventsViewHolder(view);
            }
        };
        mRecycler.setAdapter(mAdapter);
        mAdapter.startListening();

    }


}
